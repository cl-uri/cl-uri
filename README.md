cl-uri
======
## A library to parse/manipulate/use URIs

This is a library to parse/manipulate/use uris. It parses uri's according to [RFC 3986](http://tools.ietf.org/html/rfc3986).

The full [API](https://common-lisp.net/project/cl-uri/api) can be found here. Each scheme is it's own type so you can dispatch based on the scheme of the uri. A uri can be created either with the generic constructor (uri <string>) or with a scheme specific constructor (http "//common-lisp.net/projects/cl-uri/index.html" :fragment "developers").

