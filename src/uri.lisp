;;;Copyright (c) 2007, Arthur Smyles
;;;All rights reserved.
;;;
;;;Redistribution and use in source and binary forms, with or without
;;;modification, are permitted provided that the following conditions are met:
;;;
;;;1. Redistributions of source code must retain the above copyright notice,
;;;   this list of conditions and the following disclaimer.
;;;2. Redistributions in binary form must reproduce the above copyright
;;;   notice, this list of conditions and the following disclaimer in the
;;;   documentation and/or other materials provided with the distribution.
;;;
;;;         THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;;         AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;         IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;         ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;;;         LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;;         CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;;         SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;;         INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;;         CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;;         ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;;         POSSIBILITY OF SUCH DAMAGE.
;;;

(in-package #:cl-uri.common-lisp.net)
;
;uri's are special symbols that define a scheme and scheme specific part. 
;they cannot be directly called because the scheme defines the type of uri.
;see RFC #### for details

(defstruct uri 
  "Base class for all uri's"
  (scheme nil :type symbol :read-only t)
  (heir-part nil :type string :read-only t)
  (query nil :type (or null string) :read-only t)
  (fragment nil :type (or null string) :read-only t))

(defgeneric authority (uri))
(defgeneric path (uri))
(defgeneric user-info (uri))
(defgeneric host (uri))
(defgeneric port (uri))

(defmethod print-object ((obj uri) stream)
 (cond
   ((and *print-escape* *read-eval*) (format stream "#.(~S \"~@[~(~A~):~]~A~@[?~A~]~@[#~A~]\")" 'uri (uri-scheme obj) (uri-heir-part obj) (uri-query obj) (uri-fragment obj)))
   (*print-escape* (format stream "(~S \"~@[~(~A~):~]~A~@[?~A~]~@[#~A~]\")" 'uri (uri-scheme obj) (uri-heir-part obj) (uri-query obj) (uri-fragment obj)))
   (t (format stream "~@[~(~A~):~]~A~@[?~A~]~@[#~A~]" (uri-scheme obj) (uri-heir-part obj) (uri-query obj) (uri-fragment obj)))))

(defun uri (uri &aux (scheme? (and (position #\: uri) (subseq uri 0 (position #\: uri)))))
  "parses a uri into an object. If the scheme is registered then the returned object is of that type. otherwise it is a uri structure. Also supports relative uri."
  (let ((scheme (scheme scheme?))
	(scheme-body (if scheme?
		       (subseq uri (1+ (length scheme?)))
		       uri)))
    (let ((heir-part (subseq scheme-body 0 (or (position #\? scheme-body) (position #\# scheme-body))))
	  (query (and (position #\? scheme-body) (subseq scheme-body (1+ (position #\? scheme-body)) (position #\# scheme-body))))
	  (fragment (and (position #\# scheme-body) (subseq scheme-body (1+ (position #\# scheme-body))))))
      (if (or (null scheme) (keywordp scheme)) ;scheme is not registered or relative uri.
	(make-uri :scheme scheme :heir-part heir-part :query query :fragment fragment)
	(funcall scheme heir-part :query query :fragment fragment)
	))))

(defmethod authority ((uri string))
  "returns the authority component of the uri if it is heirarchical. Otherwise nil"
  (authority (uri uri)))

(defmethod authority ((uri uri))
  "returns the authority component of the uri if it is heirarchical. Otherwise nil"
  (let* ((heir-part (uri-heir-part uri)) 
	 (start-auth (search "//" heir-part))) ;;only if it starts with authority-part do we check otherwise return nil
    (when start-auth 
      (subseq heir-part 2 (position #\/ heir-part :start 2)))))


(defmethod path ((uri uri))
  "returns the path component if the uri is heirarchical"
  (let* ((heir-part (uri-heir-part uri))
	 (start-auth (search "//" heir-part))
	 (start-path 
	   (if start-auth (position #\/ heir-part :start (+ 2 start-auth)) 0))) 
    (subseq heir-part start-path)))

(defmethod path ((uri string))
  "returns the path component if the uri is heirarchical"
  (path (uri uri)))

(defmethod user-info ((uri uri))
  "returns the user-info portion of a uri if it is heirarchical"
  (let* ((authority (authority uri)) (end-userinfo (position #\@ authority)))
    (if  end-userinfo (subseq authority 0 end-userinfo))))

(defmethod port ((uri uri))
  "returns the port number in the authority component"
  (let* ((authority (authority uri)) 
	 (start-port (position #\: authority :from-end t)))
     (if start-port (parse-integer (subseq authority (1+ start-port)) :junk-allowed t))))


 (defmethod host ((uri uri))
   "returns the host" 
   (let ((authority (authority uri)))
     (subseq authority
	     (if (user-info uri)
	       (1+ (position #\@ authority)) 0)
	     (if (port uri) (position #\: authority :from-end t)))))


(defun relative? (uri)
  "Is this a relative uri"
  (not (uri-scheme uri)))

(defun heirarchical? (uri)
  "Is this a heierarchical uri"
  (char= (char (uri-heir-part uri) 0) #\/))


(defun relativize (base uri)
  (unless base (return-from relativize uri))
  (let* ((base-heir-part (subseq (uri-heir-part base) 0 (1+ (position #\/ (uri-heir-part base) :from-end t))))
	 (base? (search base-heir-part (uri-heir-part uri))))
  (if (and (heirarchical? base) (heirarchical? uri)
	   (eq (uri-scheme base) (uri-scheme uri)) 
	   base?)
    (make-uri :scheme nil
	     :heir-part (subseq (uri-heir-part uri) (+ base? (length base-heir-part)))
	     :query (uri-query uri)
	     :fragment (uri-fragment uri))
    uri)))


;TODO fix this with new api
(defun resolve (base-uri uri)
  (labels ((merge-heir-parts (base-uri uri)
	   (let ((uri-authority (authority uri))
		 (uri-path (path uri)))
	     (if uri-authority (uri-heir-part uri)
	       (let ((base-authority (authority base-uri))
		     (base-path (path base-uri)))
		 (if (char= (char uri-path 0) #\/)
		   (format nil "//~A~A" base-authority (remove-dot-segments uri-path))
		   (format nil "//~A~A" base-authority (remove-dot-segments (merge-paths base-path uri-path))))))))
	 (merge-paths (base-path relative-path)
		      (cond
			((and base-path (string= base-path ""))
			(format nil "/~A" relative-path))
			((find #\/ base-path)
			 (concatenate 'string (subseq base-path 0 (1+ (position #\/ base-path :from-end t))) relative-path))
			(t relative-path)))
	 (remove-dot-segments (path)
			      (loop for x across path
				    with previous-tokens = (make-array 5 :fill-pointer 0 :adjustable t :element-type 'character)
				    with result
				    do (progn
					 ;(break "in-loop" x previous-tokens result (arrayp previous-tokens))
					 (vector-push-extend x previous-tokens)
					 (cond 
					   ((or (string= previous-tokens "../")
						(string= previous-tokens "./"))
					    (setf (fill-pointer previous-tokens) 0))
					   ((or (string= previous-tokens "/./")
						(string= previous-tokens "/."))
					    (setf (fill-pointer previous-tokens) 0)
					    (vector-push #\/ previous-tokens))
					   ((or (string= previous-tokens "/../")
						(string= previous-tokens "/.."))
					    (setf (fill-pointer previous-tokens) 0)
					    (vector-push #\/ previous-tokens)
					    (setf result (subseq result 0 (position #\/ result :from-end t))))
					   ((or (string= previous-tokens ".") ;do nothing
						(string= previous-tokens "..")))
					   ((and (eql (char previous-tokens 0) #\/)
						 (eql (char previous-tokens (1- (length previous-tokens))) #\/))
					    (setf result (concatenate 'string result (subseq previous-tokens 0 (1- (length previous-tokens)))))
					    (setf (fill-pointer previous-tokens) 0)
					    (vector-push #\/ previous-tokens))))
				    finally (progn
					      (return (if (or (string= previous-tokens ".") (string= previous-tokens ".."))
							result
							(concatenate 'string result previous-tokens)))))))

    (unless (relative? uri) (return-from resolve uri))
    (if (keywordp (uri-scheme base-uri))
      (make-uri :scheme (uri-scheme base-uri)
	     :heir-part (merge-heir-parts base-uri uri)	
	     :query (or (uri-query uri) (uri-query base-uri))
	     :fragment (uri-fragment uri))
      (funcall (uri-scheme base-uri)
              (merge-heir-parts base-uri uri)
	     :query (or (uri-query uri) (uri-query base-uri))
	     :fragment (uri-fragment uri)))))
