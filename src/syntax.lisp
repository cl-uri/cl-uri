;;;Copyright (c) 2007, Arthur Smyles
;;;All rights reserved.
;;;
;;;Redistribution and use in source and binary forms, with or without
;;;modification, are permitted provided that the following conditions are met:
;;;
;;;1. Redistributions of source code must retain the above copyright notice,
;;;   this list of conditions and the following disclaimer.
;;;2. Redistributions in binary form must reproduce the above copyright
;;;   notice, this list of conditions and the following disclaimer in the
;;;   documentation and/or other materials provided with the distribution.
;;;
;;;         THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;;         AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;         IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;         ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;;;         LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;;         CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;;         SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;;         INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;;         CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;;         ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;;         POSSIBILITY OF SUCH DAMAGE.
;;;

(in-package #:cl-uri.common-lisp.net)

(defun set-uri-reader-syntax (uri-dmc &optional (readtable *readtable*))
  (flet ((uri-syntax (stream char prefix)
		     (declare (ignore char prefix))
		     (uri (read stream t nil t))))
    (set-dispatch-macro-character (char uri-dmc 0) (char uri-dmc 1) (function uri-syntax) readtable)))

(defmacro with-uri-syntax ((uri-dmc) &body body)
  `(let ((*readtable* (copy-readtable))
         (*print-pprint-dispatch* (copy-pprint-dispatch)))
     (set-uri-reader-syntax ,uri-dmc *readtable*)
     (set-uri-printer-syntax ,uri-dmc *print-pprint-dispatch*)
     ,@body))

(defmacro using-uri-syntax (uri-dmc)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
      (set-uri-reader-syntax ,uri-dmc)))

;this temporarily overides print-object from uri
(defun set-uri-printer-syntax (uri-dmc &optional (table *print-pprint-dispatch*))
  (flet ((uri (stream uri)
                (if *print-escape*
		  (let* ((*print-escape* nil)
			 (uri-string (with-output-to-string (s)
				       (print-object uri s))))
                    (format stream "~A~S" uri-dmc uri-string))
                    (print-object uri stream))))
    (set-pprint-dispatch 'uri (function uri) 1 table)))
