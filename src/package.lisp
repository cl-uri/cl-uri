;;;Copyright (c) 2007, Arthur Smyles
;;;All rights reserved.
;;;
;;;Redistribution and use in source and binary forms, with or without
;;;modification, are permitted provided that the following conditions are met:
;;;
;;;1. Redistributions of source code must retain the above copyright notice,
;;;   this list of conditions and the following disclaimer.
;;;   2. Redistributions in binary form must reproduce the above copyright
;;;      notice, this list of conditions and the following disclaimer in the
;;;         documentation and/or other materials provided with the distribution.
;;;
;;;         THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;;         AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;         IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;         ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;;;         LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;;         CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;;         SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;;         INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;;         CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;;         ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;;         POSSIBILITY OF SUCH DAMAGE.
;;;
(defpackage #:cl-uri.common-lisp.net
  (:nicknames #:uri)
  (:use #:common-lisp)
  (:shadow #:go #:pop)
  ;uri.lisp
  (:export #:uri #:uri-scheme #:uri-heir-part #:uri-query #:uri-fragment #:resolve #:relativize #:heirarchical? #:relative?)
  ;schemes.lisp
  (:export #:register-scheme #:define-scheme)
  ;standard schemes
  (:export #:acap #:cid #:crid #:data #:dav #:dict #:dns #:fax #:file #:ftp #:go #:gopher #:h323 #:http #:https #:im #:imap #:info #:ipp #:iris.beep #:ldap #:mailto #:mid #:modem #:mtqp #:mupdate #:news #:nfs #:nntp #:opaquelocktoken #:pop #:pres #:rtsp #:service #:sip #:sips #:snmp #:soap.beep #:soap.beeps #:tag #:tel #:telnet #:tftp #:thismessage #:tip #:urn #:vemmi #:xmlrpc.beep #:xmlrpc.beeps #:xmpp #:z39.50r #:z39.50s) 
  ;from util.lisp
  (:export #:gentag #:tag-uri #:tag-name)
  ;others from uri.lisp
  (:export #:authority #:path #:user-info #:port #:host)
  ;syntax.lisp
  (:export #:set-uri-reader-syntax #:using-uri-syntax #:with-uri-syntax #:set-uri-printer-syntax))


