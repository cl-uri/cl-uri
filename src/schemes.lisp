;;;Copyright (c) 2007, Arthur Smyles
;;;All rights reserved.
;;;
;;;Redistribution and use in source and binary forms, with or without
;;;modification, are permitted provided that the following conditions are met:
;;;
;;;1. Redistributions of source code must retain the above copyright notice,
;;;   this list of conditions and the following disclaimer.
;;;2. Redistributions in binary form must reproduce the above copyright
;;;   notice, this list of conditions and the following disclaimer in the
;;;   documentation and/or other materials provided with the distribution.
;;;
;;;         THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;;         AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;         IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;         ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;;;         LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;;         CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;;         SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;;         INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;;         CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;;         ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;;         POSSIBILITY OF SUCH DAMAGE.
;;;

(in-package #:cl-uri.common-lisp.net)

(defvar *scheme-registry* (make-hash-table :test #'equal))

(defmacro define-scheme (name (&rest slots) &optional documentation)
  `(progn 
     (defstruct (,name (:include uri (scheme ',name))
		    (:constructor ,name (heir-part &key query fragment)))
     ,@(when documentation (list documentation)))
     ,@(loop for (method (uri . args) . body) in slots
	     collect `(defmethod ,method ((,uri ,name) ,@args) ,@body) )))


;primarily used by people who make their own schemes
(defun register-scheme (name sym)
  (setf (gethash (string-downcase name) *scheme-registry*) sym))

(defun scheme (name)
  (flet ((read-case (str)
		    (ecase (readtable-case *readtable*)
		      (:upcase (string-upcase str))
		      (:downcase (string-downcase str))
		      (:preserve str)
		      (:invert str))))
  (and name (or (find-symbol (read-case name) :cl-uri.common-lisp.net) (gethash (string-downcase name) *scheme-registry*) (intern (read-case name) :keyword)))))

(define-scheme acap ()  "application configuration access protocol  [RFC2244]")

(define-scheme cid 
	       ((authority (uri)
			   (let* ((heir-part (uri-heir-part uri))
				 (start-auth (search "@" heir-part)))
			     (when start-auth
			       (subseq heir-part (1+ start-auth)))))
		(user-info (uri)
			   (let* ((heir-part (uri-heir-part uri))
				 (start-auth (search "@" heir-part)))
			     (when start-auth
			       (subseq heir-part 0 start-auth)))))
	       "content identifier [RFC2392]")

(define-scheme crid ()	"TV-Anytime Content Reference Identifier 	[RFC4078]")
(define-scheme data () "data 	[RFC2397]")
(define-scheme dav ()	"dav 	[RFC2518]")
(define-scheme dict ()	"dictionary service protocol 	[RFC2229]")
(define-scheme dns ()	"Domain Name System 	[RFC4501]")
(define-scheme fax ()	"fax 	[RFC3966]")
(define-scheme file ()	"Host-specific file names 	[RFC1738]")
(define-scheme ftp ()	"File Transfer Protocol 	[RFC1738]")
(define-scheme go ()	"go 	[RFC3368]")
(define-scheme gopher () "The Gopher Protocol 	[RFC4266]")
(define-scheme h323 ()	"H.323 	[RFC3508]")
(define-scheme http ()	"Hypertext Transfer Protocol 	[RFC2616]")
(define-scheme https ()	"Hypertext Transfer Protocol Secure 	[RFC2818]")
(define-scheme im ()	"Instant Messaging 	[RFC3860]")
(define-scheme imap ()	"internet message access protocol 	[RFC2192]")
(define-scheme info ()	"Information Assets with Identifiers in Public Namespaces 	[RFC4452]")
(define-scheme ipp ()	"Internet Printing Protocol 	[RFC3510]")
(define-scheme iris.beep () "iris.beep 	[RFC3983]")
(define-scheme ldap ()	"Lightweight Directory Access Protocol 	[RFC4516]")
(define-scheme mailto () "Electronic mail address 	[RFC2368]")
(define-scheme mid ()	"message identifier 	[RFC2392]")
(define-scheme modem ()	"modem 	[RFC3966]")
(define-scheme mtqp ()	"Message Tracking Query Protocol 	[RFC3887]")
(define-scheme mupdate () "Mailbox Update (MUPDATE) Protocol 	[RFC3656]")
(define-scheme news ()	"USENET news 	[RFC1738]")
(define-scheme nfs ()	"network file system protocol 	[RFC2224]")
(define-scheme nntp ()	"USENET news using NNTP access 	[RFC1738]")
(define-scheme opaquelocktoken () "opaquelocktokent 	[RFC2518]")
(define-scheme pop ()	"Post Office Protocol v3 	[RFC2384]")
(define-scheme pres ()	"Presence 	[RFC3859]")
(define-scheme rtsp ()	"real time streaming protocol 	[RFC2326]")
(define-scheme service () "service location 	[RFC2609]")
(define-scheme sip ()	"session initiation protocol 	[RFC3261]")
(define-scheme sips ()	"secure session initiation protocol 	[RFC3261]")
(define-scheme snmp ()	"Simple Network Management Protocol 	[RFC4088]")
(define-scheme soap.beep () "soap.beep 	[RFC3288]")
(define-scheme soap.beeps () "soap.beeps 	[RFC3288]")
(define-scheme tag ()	"tag 	[RFC4151]")
(define-scheme tel ()	"telephone 	[RFC3966]")
(define-scheme telnet () "Reference to interactive sessions 	[RFC4248]")
(define-scheme tftp ()	"Trivial File Transfer Protocol 	[RFC3617]")
(define-scheme thismessage () "multipart/related relative reference resolution 	[RFC2557]")
(define-scheme tip () "Transaction Internet Protocol 	[RFC2371]")
(define-scheme urn ()	"Uniform Resource Names (click for registry) 	[RFC2141]")
(define-scheme vemmi ()	"versatile multimedia interface 	[RFC2122]")
(define-scheme xmlrpc.beep ()	"xmlrpc.beep 	[RFC3529]")
(define-scheme xmlrpc.beeps ()	"xmlrpc.beeps 	[RFC3529]")
(define-scheme xmpp ()	"Extensible Messaging and Presence Protocol 	[RFC4622]")
(define-scheme z39.50r () "Z39.50 Retrieval 	[RFC2056]")
(define-scheme z39.50s () "Z39.50 Session  	[RFC2056])")
